var gulp = require("gulp");
var babel = require("gulp-babel");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var del = require("del");
var seq = require("run-sequence");
var uglify = require("gulp-uglify");

var config = require("./build_config.json");

gulp.task("js:babel", function(){
    return gulp.src(config.paths.source.js + "**/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel(config.babel))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(config.paths.build.js));
});

gulp.task("js:concat", function(){
    return gulp.src(config.paths.build.js + "**/*.js")
        .pipe(concat(config.output.js))
        .pipe(uglify())
        .pipe(gulp.dest(config.paths.dist.js));
});


gulp.task("default", function(cb){
    seq(["js:babel"],["js:concat"],cb);
})


gulp.task("clean", function(){
    del(config.clean);
});